
public class SubstringForKids {

	public static void main(String[] args) {
				System.out.println(substringForKids(1,1,"fun")); //i==j
				System.out.println(substringForKids(0,5,"peterisshouting")); //i<j
				System.out.println(substringForKids(5, 3, "bad")); // i>j
				System.out.println(substringForKids(-1, 3, "bad")); // i is negative
				System.out.println(substringForKids(1, -3, "bad")); // j is negative
	}
	
	public static String substringForKids(int i, int j, String sentence) {
		String newSent= "";
		
		if(i<0 || j<0)
		{
			return "You gave me invalid numbers";
		}
		
		else{
		
		
		        if(i<j || i==j)
		            {
			             for(int start=i; start<=j;start++)
				             {
				                 newSent+=sentence.charAt(start);
					         }
		
		             }
		
		        else if(i>j )
		            {
			           return "You gave me invalid numbers!";
			
		            }
		
		return newSent;
		
		}
		
		
	}
}