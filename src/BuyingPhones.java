import java.util.Scanner;

public class BuyingPhones {

	public static void main(String[] args) {
		System.out.println("Enter the regular price of a phone: ");
		Scanner s = new Scanner(System.in);
		double price = s.nextDouble();  //price of 1st and 2nd phones
		double discount= price*0.5;
		double thirdPrice= price-discount; // price of 3rd phone
		System.out.println("Phone 1 - $"+price);
		System.out.println("Phone 2 - $"+price);
		System.out.println("Phone 1 - $"+thirdPrice);

	}

}